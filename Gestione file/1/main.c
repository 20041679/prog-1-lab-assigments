#include <stdio.h>

#include <errno.h>
#include <string.h>
#define err(s) fprintf(stderr, s "%s\n", strerror(errno))

#define F1_STR "1.txt"
#define F2_STR "2.txt"

int main(void) {
    int ret = 0;
    FILE *f1;
    if ((f1 = fopen(F1_STR, "r"))) {

        FILE *f2;
        if ((f2 = fopen(F2_STR, "r"))) {

            char c1;
            char c2;
            int diff = 0;
            while (fscanf(f1, "%c", &c1) != EOF &&
                   fscanf(f2, "%c", &c2) != EOF &&
                   !diff) {
                if (c1 != c2) {
                    diff = 1;
                    printf("file 1: %c, file2: %c\n", c1, c2);
                }
            }
            
            fclose(f2);

        } else {
            ret = 1;
            err(F2_STR ": ");
        }

        fclose(f1);
    } else {
        ret = 1;
        err(F1_STR ": ");
    }

    return ret;
}
