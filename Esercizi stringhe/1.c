#include <stdio.h>
#include <string.h>

#define BUF_SIZE 4096
#define ARR_SIZE(a) (sizeof(a) / sizeof(*a))

void str_strip_trailing(char *s) {
    char *pos = strchr(s, '\n');
    if (pos != NULL) { *pos = '\0'; }
}

int main(void) {
    char str1[BUF_SIZE];
    char str2[BUF_SIZE];

    fgets(str1, ARR_SIZE(str1), stdin);
    str_strip_trailing(str1);
    fgets(str2, ARR_SIZE(str2), stdin);
    str_strip_trailing(str2);

    if (strcmp(str1, str2) == 0) {
        puts("Le due stringhe sono uguali");
    } else if (strstr(str1, str2) != NULL) {
        puts("La seconda stringa e' contenuta nella prima");
    } else if (strstr(str2, str1) != NULL) {
        puts("La prima stringa e' contenuta nella seconda");
    }

    return 0;
}
