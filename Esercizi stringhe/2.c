#include <stdio.h>
#include <string.h>

#define BUF_SIZE 4096
#define ARR_SIZE(a) (sizeof(a) / sizeof(*a))

const char vowels[] = "aeiouAEIOU";

void str_strip_trailing(char *s) {
    char *pos = strchr(s, '\n');
    if (pos != NULL) { *pos = '\0'; }
}

void change_forbidden_chars(char *s, const char *remove, char new) {
    char *match = s;
    while ((match = strpbrk(match, remove)) != NULL) {
        *match = new;
    }
}

int main(void) {
    char str1[BUF_SIZE];
    char str2[BUF_SIZE];
    char str12[BUF_SIZE * 2 - 1];

    fgets(str1, ARR_SIZE(str1), stdin);
    str_strip_trailing(str1);
    fgets(str2, ARR_SIZE(str2), stdin);
    str_strip_trailing(str2);

    strcpy(str12, str1);
    strcat(str12, str2);

    change_forbidden_chars(str12, vowels, '*');

    puts(str12);
}
