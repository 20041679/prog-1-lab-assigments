#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define BUF_SIZE 100
#define ARR_SIZE(a) (sizeof(a) / sizeof(*a))

const char vowels[] = {'a', 'e', 'i', 'o', 'u' };

void str_strip_trailing(char *s) {
    char *pos = strchr(s, '\n');
    if (pos != NULL) { *pos = '\0'; }
}

void count_vowels(char *s, unsigned int counts[]) {
    while (*s) {
        int match = 0;
        size_t i;
        for (i = 0; i < ARR_SIZE(vowels) && !match; ++i) {
            if (vowels[i] == tolower(*s)) { match = 1; }
        }
        if (match) { ++counts[i - 1]; }
        ++s;
    }
}

unsigned int arr_sum(unsigned int arr[], size_t len) {
    unsigned int ret = 0;
    for (size_t i = 0; i < len; ++i) { ret += arr[i]; }
    return ret;
}

int main(void) {
    char str[BUF_SIZE];

    fgets(str, ARR_SIZE(str), stdin);
    str_strip_trailing(str);

    unsigned int vowels_counters[5] = {0};
    count_vowels(str, vowels_counters);
    
    unsigned int n_vowels = arr_sum(vowels_counters, ARR_SIZE(vowels_counters));
    size_t len = strlen(str);
    unsigned int n_consonants = len - n_vowels;

    printf("vocali: %u, consonanti: %u\n", n_vowels, n_consonants);
    puts("frequenze vocali:");
    for (size_t i = 0; i < 5; ++i) {
        printf("%c: %lf\n", vowels[i], ((double)vowels_counters[i] / len) * 100);
    }
}
