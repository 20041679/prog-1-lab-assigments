#include <stdio.h>

#define ARR_SIZE(arr) (sizeof(arr) / sizeof(*arr))

#define MARK -1

int write_to_file(char *filename, int arr[]) {
    int ret = 0;

    FILE *file;
    if ((file = fopen(filename, "w")) == NULL) {
        ret = -1;
    } else {
        int i = 0;
        while (arr[i] != MARK && arr[i + 1] != MARK) {
            fprintf(file, "%d-", arr[i]);

            ++i;
        }
        fprintf(file, "%d", arr[i]);

        fclose(file);
    }

    return ret;
}

int read_from_file(char *filename, int arr[], int size) {
    int ret;
    FILE *file;
    if ((file = fopen(filename, "r")) == NULL) {
        ret = -1;
    } else {
        int i = 0;
        int end = 0;
        int buf;
        while (i < size && !end) {
            fscanf(file, "%d", &buf);
            if (buf == MARK) {
                end = 1;
            } else {
                arr[i] = buf;
                ++i;
            }
        }
        if (end) {
            arr[i] = MARK;
            ret = i;
        } else {
            arr[0] = MARK;
            ret = -1;
        }

        fclose(file);
    }

    return ret;
}

void modify(int arr1[], int arr2[]) {
    int i = 0;
    while (arr1[i] != MARK && arr2[i] != MARK) {
        int a = arr1[i];
        int b = arr2[i];
        if (a == b) {
            arr1[i] = 0;
        } else if (a < b) {
            arr1[i] = b;
        } else {
            ++arr1[i];
        }
        ++i;
    }
}

int main(void) {
    int ret = 0;

    int arr1[10];
    if (read_from_file("in1.txt", arr1, ARR_SIZE(arr1)) < 0) {
        ret = 1;
    } else {
        int arr2[10];

        if (read_from_file("in2.txt", arr2, ARR_SIZE(arr2)) < 0) {
            ret = 1;
        } else {
            modify(arr2, arr1);
            write_to_file("out.txt", arr2);
        }
    }

    return ret;
}
