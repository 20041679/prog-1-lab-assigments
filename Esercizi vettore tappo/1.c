#include <stdio.h>
#include <assert.h>

#define MARKER -1
#define ARR_SIZE(a) (sizeof(a) / sizeof(*a))

int populate(int arr[], size_t size) {
    int insert;
    size_t i = 0;
    while (i < size && ((scanf("%d", &insert)), insert != MARKER)) {
        arr[i] = insert;
        ++i;
    }

    int ret;
    if (i < size) {
        arr[i] = MARKER;
        ret = 1;
    }
    else { ret = -1; }
    return ret;
}

void print(int arr[]) {
    size_t i = 0;
    if (arr[i] != MARKER) {
        while (arr[i + 1] != MARKER) {
            printf("%d*", arr[i]);
            ++i;
        }
        printf("%d", arr[i]);
    }
}

int *find(int where[], int what) {
    size_t i = 0;
    int found = 0;
    while (where[i] != MARKER && !found) {
        if (where[i] == what) { found = 1; }
        else { ++i; }
    }

    int *ret;
    if (found) { ret = &where[i]; }
    else { ret = NULL; }
    return ret;
}

void remove_first(int where[], int what) {
    assert(what != MARKER);

    int *pos = find(where, what);
    if (pos != NULL) {
        while ((*pos) != MARKER) {
            *pos = *(pos + 1);
            ++pos;
        }
    }
}

void remove_all(int where[], int what) {
    size_t shift_width = 0;
    size_t i = 0;
    while (where[i + shift_width] != MARKER) {
        if (where[i] == what) {
            ++shift_width;
        } else {
            ++i;
        }
        where[i] = where[i + shift_width];
    }
    where[i] = MARKER;
}

int main(void) {
    int array[10];
    if (populate(array, ARR_SIZE(array))) {
        remove_all(array, 5);
        print(array);
    }
}
