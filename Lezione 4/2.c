#include <stdio.h>
#include <math.h>

#define PERC_CRASHED (+0.12)
#define PERC_NOT_CRASHED (-0.04)

int main(void) {
    double new_total_amount = 0;
    double old_total_amount = 0;
    do {
        puts("Inserire numero di auto");
        unsigned int n_cars;
        scanf("%u", &n_cars);

        double old_client_amount = 0;
        double new_client_amount = 0;
        for (unsigned int i = 0; i < n_cars; i++) {
            double old_car_amount;
            double new_car_amount;

            printf("Inserire importo dell'auto %u\n", i + 1);
            do {
                scanf("%lf", &old_car_amount);
            } while (old_car_amount < 0);

            old_client_amount += old_car_amount;

            puts("Inserire numero di incidenti dell'anno scorso");
            int n_crashes;
            do {
                scanf("%u", &n_crashes);
            } while (n_crashes < 0);

            if (n_crashes > 0) {
                new_car_amount = old_car_amount + PERC_CRASHED * old_car_amount;
            } else {
                new_car_amount = old_car_amount - PERC_NOT_CRASHED * old_car_amount;
            }

            new_client_amount += new_car_amount;
        }
        printf("Importo da pagare per il cliente: %lf\n", new_client_amount);

        old_total_amount += old_client_amount;
        new_total_amount += new_client_amount;

        puts("Inserire nuovo cliente? y/n");
    } while (getchar() == 'y');

    printf("Totale degli importi: %lf, variazione rispetto al precedente: %lf\n",
            new_total_amount, fabs(new_total_amount - old_total_amount));
}
