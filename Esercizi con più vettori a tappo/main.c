#include <stdio.h>

#define MARK -1
#define LEN(x) (sizeof(x) / sizeof(*x))

void copy(int to[], int from[], int n, int *ret) {
    int i = 0;
    while (from[i] != MARK && i < n - 1) {
        to[i] = from[i];
        ++i;
    }
    to[i] = MARK;

    *ret = from[i] == MARK ? 1 : -1;
}

void cmp(int a1[], int a2[], int *ret) {
    int i = 0;
    while (a1[i] != MARK && a2[i] != MARK && a1[i] == a2[i]) { ++i; }

    *ret = a1[i] == a2[i] ? 1 : -1;
}

int count_greater(int a1[], int a2[]) {
    int i = 0;
    int ret = 0;
    while (a1[i] != MARK && a2[i] != MARK) {
        if (a1[i] > a2[i]) { ++ret; }
        ++i;
    }
    while (a2[i] != MARK) {
        ++ret;
        ++i;
    }
    return ret;
}

int copy_even_positions(int to[], int from[], int n) {
    int i = 0;
    while (from[i * 2] && from[(i * 2) + 1] != MARK && i < n - 1) {
        to[i] = from[i * 2];
        ++i;
    }
    to[i] = MARK;
    return i;
}

int sum(int a1[], int a2[], int n, int sum[]) {
    int i = 0;
    while (a1[i] != MARK && a2[i] != MARK && i < n - 1) {
        sum[i] = a1[i] + a2[i];
        ++i;
    }
    while (a1[i] != MARK && i < n - 1) {
        sum[i] = a1[i];
        ++i;
    }
    while (a2[i] != MARK && i < n - 1) {
        sum[i] = a2[i];
        ++i;
    }

    sum[i] = MARK;

    return a1[i] == MARK || a2[i] == MARK ? 1 : -1;
}
