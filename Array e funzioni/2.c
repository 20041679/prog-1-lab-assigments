#include <stdio.h>

void print_int_array(int arr[], size_t len) {
    for (size_t i = 0; i < len; ++i) {
        printf("%d", arr[i]);
        if (i < len - 1) { printf(", "); }
    }
    puts("");
}
