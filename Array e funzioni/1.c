#include <stdio.h>

void input_int_array(int arr[], size_t len) {
    for (size_t i = 0; i < len; ++i) {
        scanf("%d", arr[i]);
    }
}
