#include <assert.h>

void add_int_array(int A[], int dimA, int B[], int dimB, int C[], int dimC) {
    assert(dimA == dimB);
    assert(dimB == dimC);

    for (int i = 0; i < dimA; ++i) {
        C[i] = A[i] + B[i];
    }
}
