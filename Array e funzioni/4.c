int sum_int_array(int arr[], int len) {
    int ret = 0;
    for (int i = 0; i < len; i++) {
        ret += arr[i];
    }
    return ret;
}

int f(int A[], int dimA, int B[], int dimB) {
    int sumi = 0;
    for (int i = 0; i < dimA; ++i) { sumi += A[i] * sum_int_array(B, dimB); }
    return sumi;
}
