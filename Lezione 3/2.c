#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#define END -1000

#define ASC_STR "crescente"
#define DESC_STR "decrescente"
#define NEITHER_STR "ne' " ASC_STR "ne' " DESC_STR

const char *sequence_monotony_str(bool asc, bool desc) {
    const char *sequence_kind_str;
    printf("la sequenza e' ");
    if (asc) {
        assert(!desc);
        sequence_kind_str = ASC_STR;
    } else if (desc) {
        assert(!asc);
        sequence_kind_str = DESC_STR;
    } else {
        sequence_kind_str = NEITHER_STR;
    }
    return sequence_kind_str;
}

int main(void) {
    int sum = 0;
    int prev, num;
    bool asc = true, desc = true;
    unsigned num_counter = 0;

    scanf("%d", &prev);
    sum += prev;

    if (prev != END) {
        num_counter = 1;
        while (scanf("%d", &num), num != END) {
            ++num_counter;
            sum += num;
            asc &= num > prev;
            desc &= num < prev;

            prev = num;
        }
    }


    if (num_counter > 1) {
        puts(sequence_monotony_str(asc, desc));
    } else {
        puts("la sequenza non e' abbastanza lunga per essere definita crescente o decrescente");
    }
    
    printf("somma: %d\n", sum);

    return 0;
}
