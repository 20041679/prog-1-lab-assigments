#include <stdio.h>

void print_repeat_char(char c, unsigned n) {
    for (unsigned i = 0; i < n; ++i) {
        printf("%c", c);
    }
}

int main(void) {
    puts("Lato del quadrato");
    unsigned side;
    scanf("%u", &side);

    for (unsigned row = 1; row <= side; ++row) {
        unsigned stars_amount = row;
        print_repeat_char('*', stars_amount);

        unsigned plus_amount = side - stars_amount;
        print_repeat_char('+', plus_amount);

        puts("");
    }

    return 0;
}
