#include <stdio.h>

void minmax(int array[], int len, int *min, int *max) {
    int i = 0;
    *min = array[i];
    *max = array[i];

    for (i = 1; i < len; i++) {
        if (array[i] < *min) { *min = array[i]; }
        if (array[i] > *max) { *max = array[i]; }
    }
}
