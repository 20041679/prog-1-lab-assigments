#include <stdio.h>

int max(int array[], int len, int *n_max_occurr) {
    int max;
    *n_max_occurr = 0;
    int i = 0;
    max = array[i];
    (*n_max_occurr)++;

    for (i = 1; i < len; i++) {
        if (array[i] == max) { (*n_max_occurr)++; }
        else if (array[i] > max) {
            max = array[i];
            (*n_max_occurr) = 1;
        }
    }
    return max;
}
