#include <stdio.h>

int maxcount(int array[], int len) {
    int i = 0;
    int max_i = i;

    for (i = 1; i < len; i++) {
        if (array[i] > array[max_i]) { max_i = i; }
    }

    int ret = array[max_i];
    array[max_i] = -1;
    return ret;
}
