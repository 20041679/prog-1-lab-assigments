#include <stdio.h>

size_t slen(char *s) {
    size_t i = 0;
    while (s[i] != '\0') { ++i; }
    return i;
}

void strrev(char *src, char *dst) {
    size_t len = slen(src);
    for (size_t i = 0; i < len; ++i) {
        dst[i] = src[len - 1 - i];
    }
    src[len] = '\0';
}

int main(void) {
    char s[] = "Informatica";
    char s_rev[sizeof(s)];

    strrev(s, s_rev);
    printf("%s", s_rev);
}
