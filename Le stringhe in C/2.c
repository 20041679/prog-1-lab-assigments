size_t lunghezza(char *s) {
    size_t i = 0;
    while (s[i] != '\0') { ++i; }
    return i;
}

int palindroma(char *s) {
    int is_palindrome = 1;
    size_t len = lunghezza(s);
    for (size_t i = 0; i < len / 2 && is_palindrome; ++i) {
        if (s[i] != s[len - 1 - i]) { is_palindrome = 0; }
    }
    return is_palindrome;
}

void cswap(char *c1, char *c2) {
    char temp = *c1;
    *c1 = *c2;
    *c2 = temp;
}

void inverti(char *s) {
    size_t len = lunghezza(s);
    for (size_t i = 0; i < len / 2; ++i) {
        cswap(&s[i], &s[len - 1 - i]);
    }
}
